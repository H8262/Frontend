import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { IndexModule } from './index/index.module';
import { OutdoorModule } from './outdoor/outdoor.module';
import { IndoorModule} from './indoor/indoor.module'
import { UsageModule } from './usage/usage.module'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IndexModule,
    OutdoorModule,
    IndoorModule,
    UsageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
