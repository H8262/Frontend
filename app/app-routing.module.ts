import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { IndexComponent } from './index/index.component';
import { OutdoorComponent } from './outdoor/outdoor.component';
import { IndoorComponent } from './indoor/indoor.component';
import { UsageComponent } from './usage/usage.component';

/*
*  Defines available application root routes.
*/
const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: OutdoorComponent
  },
  {
    path: 'outdoor',
    component: OutdoorComponent
  },
  {
    path: 'indoor/:device',
    component: IndoorComponent
  },
  {
    path: 'usage',
    component: UsageComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

/*
*  Exports the available root routes.
*/
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }
