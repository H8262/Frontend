import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IntervalObservable } from "rxjs/observable/IntervalObservable";

import { StompService } from '@stomp/ng2-stompjs';

import { DeviceService } from '../services/device.service';
import Device from '../models/device.model';
import { STOMP_DI_CONFIG } from '../core/config/stomp.config';
import { getDefaultService } from 'selenium-webdriver/opera';

@Component({
  selector: 'app-indoor',
  templateUrl: './indoor.component.html',
  styleUrls: ['./indoor.component.css']
})
export class IndoorComponent implements OnInit {
  private deviceMac: string;
  private deviceId: string;
  private subscription: any;
  private alive: boolean;
  private display: boolean;
  public device: Device;

  constructor(private route: ActivatedRoute, private stomp: StompService, private deviceService: DeviceService) {
    this.alive = true;
    this.display = false;
    this.device = new Device;
    this.device.position = { x: 0, y: 0 };
    
    

    this.route.params.subscribe(params => {
      this.deviceId = params['device'];
      
    });
    this.getDevice()
    console.log("mac osoite: " + this.deviceMac);
    let stomp_subscription = this.stomp.subscribe('/exchange/device.location.data/motikka.' + this.deviceMac + '.data');

    stomp_subscription.map((message) => {
      return message.body;
    }).subscribe((msg_body: string) => {
      // TODO: Add checks.
      const data = JSON.parse(msg_body);
      this.display = true;
      this.device.position = data;
    });
  }

  private getDevice(): Promise<void> {
    return this.deviceService.getDevice(this.deviceId)
      .map(devices => {
        this.device = devices[0];
        this.deviceMac = this.device.ruuviTag;
      })
      .toPromise()
      .catch(error => {
        console.log(error);
      })
  }
  ngOnInit() {
    
  }

}
