import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentage'
})
export class PercentagePipe implements PipeTransform {

  transform(value: number): string {
    if (value < 0) return '0%';
    else if (value > 1) return '93%';
    return Math.floor(value * 100) + '%';
  }

}
