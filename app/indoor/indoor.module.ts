import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module'
import { MainNavModule } from '../main-nav/main-nav.module';

import { StompService, StompConfig } from '@stomp/ng2-stompjs';
import { STOMP_CONFIG, STOMP_DI_CONFIG } from '../core/config/stomp.config'

import { IndoorComponent } from './indoor.component';
import { UsageService } from '../usage/usage.service';
import { UsageComponent } from '../usage/usage.component';
import { ChartsModule } from 'ng2-charts';


import { DeviceService } from '../services/device.service';
import { PercentagePipe } from './percentage.pipe';

const stompConfig: StompConfig = {
  // Which server?
  url: STOMP_DI_CONFIG.endpoint,

  // Headers
  // Typical keys: login, passcode, host
  headers: STOMP_DI_CONFIG.headers,

  // How often to heartbeat?
  // Interval in milliseconds, set to 0 to disable
  heartbeat_in: STOMP_DI_CONFIG.heartbeat_in, // Typical value 0 - disabled
  heartbeat_out: STOMP_DI_CONFIG.heartbeat_out, // Typical value 20000 - every 20 seconds

  // Wait in milliseconds before attempting auto reconnect
  // Set to 0 to disable
  // Typical value 5000 (5 seconds)
  reconnect_delay: STOMP_DI_CONFIG.reconnect_delay,

  // Will log diagnostics on console
  debug: true
};

@NgModule({
  imports: [
    CoreModule,
    MainNavModule,
    ChartsModule
  ],
  providers: [
    DeviceService,
    {
      provide: StompConfig,
      useValue: stompConfig
    },
    StompService, UsageService
  ],
  declarations: [IndoorComponent, PercentagePipe,UsageComponent]
})
export class IndoorModule { }
