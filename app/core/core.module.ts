import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { APP_CONFIG, APP_DI_CONFIG } from './config/app.config'
import { HttpClientService } from './http-client.service'

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  exports: [
    CommonModule
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG
    },
    HttpClientService
  ],
  declarations: []
})
export class CoreModule { }
