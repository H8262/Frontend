export class OutdoorLocation {
  lng: number;
  lat: number;
}

export default OutdoorLocation;
