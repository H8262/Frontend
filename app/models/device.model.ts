import OutdoorLocation from './outdoor-location.model'
import IndoorLocation from './indoor-location.model'

export class Device {
    id: number;
    name: string;
    status: string;
    location: OutdoorLocation;
    position: IndoorLocation;
    ruuviTag: string;
}

export default Device;
