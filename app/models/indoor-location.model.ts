export class IndoorLocation {
  x: number;
  y: number;
}

export default IndoorLocation;
