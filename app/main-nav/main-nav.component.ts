import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  loggedIn: boolean;

  constructor(private router: Router) {
    if (localStorage.getItem('user')) {
      this.loggedIn = true;
    }
  }

  ngOnInit() {
  }

}
