import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module'
import { MainNavModule } from '../main-nav/main-nav.module';

import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

import { OutdoorComponent } from './outdoor.component';
import { AgmMapComponent } from '../agm-map/agm-map.component';

import { DeviceService } from '../services/device.service';

import { APP_DI_CONFIG } from '../core/config/app.config'
import { DeviceDetailComponent } from '../device-detail/device-detail.component'

import { LocationService } from "../location.service";

@NgModule({
  imports: [
    CoreModule,
    MainNavModule,
    AgmCoreModule.forRoot({
      apiKey: APP_DI_CONFIG.agmApiKey
    }),
    AgmJsMarkerClustererModule
  ],
  providers: [
    DeviceService,
    LocationService
  ],
  declarations: [
    OutdoorComponent,
    AgmMapComponent,
    DeviceDetailComponent
  ]
})
export class OutdoorModule { }
