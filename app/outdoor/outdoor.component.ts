import { Component, OnInit, ViewChild } from '@angular/core';

import { DeviceService } from '../services/device.service';
import { Device } from '../models/device.model';
import { AgmMapComponent} from '../agm-map/agm-map.component';

@Component({
  selector: 'app-motrak',
  templateUrl: './outdoor.component.html',
  styleUrls: ['./outdoor.component.css']
})
export class OutdoorComponent implements OnInit {
  private devices: Device[];
  private motikkaid: any;
  @ViewChild(AgmMapComponent) agmMap;
  constructor(private deviceService: DeviceService) { }

  ngAfterViewInit(){
    this.getDevices();
  }
  ngOnInit() {
    
    //this.agmMap.toIndoor();
  }

  public changeRoute(id: any):void{
    console.log('Route: ' + id);
    this.motikkaid = id;
  }

  private getDevices(): Promise<void> {
    return this.deviceService.getDevices()
      .map(devices => {
        this.devices = devices;
      })
      .toPromise()
      .catch(error => {
        console.log(error);
      })
  }

}
