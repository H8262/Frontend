import { Component, OnInit, Input } from '@angular/core';
import { LocationService } from "../location.service";

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit {


  private markers: any;
  selectedDiv = false;
  buttonContent = "Katso kaikki";
  searchResult = [];

  constructor(private location: LocationService) { }

  ngOnInit() {
    //this.location.obsMessage.subscribe(location => this.location2 = location);
  }
  @Input()
  set devices(data: any) {
    this.markers = data;
    for(var i; i > data.length; i++){
      if(data[i].status == "laina"){
        // push into laina array
      }

    }
  }
  revealList(){

    if(this.buttonContent == "Katso kaikki"){
    this.buttonContent = "Piilota";
    this.selectedDiv = true;
    }
    else{
      this.buttonContent = "Katso kaikki";
      this.selectedDiv = false;
    }
  }
  search(search){
    this.searchResult = [];
    for(var i = 0; i < this.markers.length; i++){
      if(this.markers[i].id.toString().includes(search) || this.markers[i].name.includes(search))
        {
          this.searchResult.push(this.markers[i]);
        }
      }
  }
  getLocation(lng,lat){
    console.log("Lat: " + lat, "Lng: " + lng);
    this.location.setLocation(lat, lng);
  }

}