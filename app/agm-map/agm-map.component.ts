import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Device } from '../models/device.model';
import { LocationService } from "../location.service";
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-agm-map',
  templateUrl: './agm-map.component.html',
  styleUrls: ['./agm-map.component.css']
})
export class AgmMapComponent implements OnInit {
  private defaultPos: any;
  private markers: any;
  selectedMarker: Device;
  searchResult = [];

  public position: LocationService;

  constructor(private location: LocationService) {  }
  @ViewChild(AgmMap) private myMap: any;

  ngOnInit() {
    this.defaultPos = {
      lat: 62.240457,
      lng: 25.760549
    };
  }

  @Input()
  set mapMarkers(data: any) {
    this.markers = data;
  }

  @Output() onMotikkaSelect: EventEmitter<any> = new EventEmitter<any>();
openedWindow : number = 0;

  onSelect(device: Device){
    /*
    this.selectedMarker = device;
    this.openedWindow = device.id;
    console.log(device.id);*/
    this.location.getLocation();
    console.log("Apua: " +  device.location.lat);
    this.myMap._mapsWrapper.setCenter({lat: device.location.lat, lng: device.location.lng});
    this.myMap._mapsWrapper.setZoom(15);
    this.toIndoor(device.id);

    
}

  isInfoWindowOpen(id){
    return this.openedWindow == id;
  }
  search(search){
    this.searchResult = [];
    for(var i = 0; i < this.markers.length; i++){
      if(this.markers[i].id.toString().includes(search) || this.markers[i].name.toUpperCase().includes(search.toUpperCase()))
        {
          if (this.markers[i].environment === "null") {
            this.markers[i].environment = "Lainassa";
          }
          this.searchResult.push(this.markers[i]);
          
        }
      }
}
private toIndoor(id){
    this.onMotikkaSelect.emit(id);
  }
}