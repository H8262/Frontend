import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import { HttpClientService } from '../core/http-client.service';
import Device from '../models/device.model';
import IndoorLocation from '../models/indoor-location.model';

@Injectable()
export class DeviceService {
  constructor(private http: HttpClientService) { }

  /*
  *  Retrieves devices from the backend.
  */
  getDevices(): Observable<Device[]> {
    return this.http.get('?query={motikkas{id,name,environment,location{lng,lat}}}')
      .map((response: Response) => {
        return response.json().data.motikkas;
      });
  };
  getDevice(motikkaid): Observable<Device[]> {
    
    return this.http.get('?query={motikkas(id:'+ motikkaid +'){id,ruuviTag,location{lng,lat}}}')
      .map((response: Response) => {
        return response.json().data.motikkas;

      });
  };

  getDeviceLocation(device): Observable<IndoorLocation> {
    return this.http.get('?query={location(device: "' + device + '"){x,y}}')
      .map((response: Response) => {
        return response.json().data.location;
      });
  };
}
