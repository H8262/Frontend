import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {ApplicationRef} from '@angular/core';

@Injectable()
export class LocationService {

    private currentLocation = new BehaviorSubject({});

    obsMessage = this.currentLocation.asObservable();

    public location = {
        lat: null,
        lng: null
    };

    constructor(private appRef: ApplicationRef){ }

    setLocation(lati,long){
        this.location = {
            lat: lati,
            lng: long
            
        };
        console.log("Set" + this.location.lat);


        //this.appRef.tick();
    }
    getLocation(){
        console.log("Got location" + this.location.lat);
        return this.location;
        
    }

}