import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsageService } from './usage.service';
import { CoreModule } from '../core/core.module'
import { APP_DI_CONFIG } from '../core/config/app.config'
import { UsageComponent } from "./usage.component";
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ChartsModule
  ],
  providers: [
    UsageService
  ],
  declarations: [
    
  ]
})
export class UsageModule {

 }
