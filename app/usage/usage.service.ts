import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import { HttpClientService } from '../core/http-client.service';
import { Usage } from '../models/usage.model';

@Injectable()
export class UsageService {
  constructor(private http: HttpClientService) { }

  /*
  *  Retrieves devices from the backend.
  */
  getUsage(ruuviMac): Observable<Usage[]> {
    
    ruuviMac = ruuviMac.toString();
    console.log("service " + ruuviMac);
    return this.http.get('?query={usageData(ruuvitag:"' + ruuviMac +'"){id,date,data,ruuvitag}}')
      .map((response: Response) => {
        return response.json().data.usageData;
      });
  }
}