import { Component, Input, OnInit } from '@angular/core';
import { Usage } from '../models/usage.model';
import { UsageService } from './usage.service'
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-usage',
  templateUrl: './usage.component.html',
  styleUrls: ['./usage.component.css']
})
export class UsageComponent implements OnInit {
  private devices: Usage[];
  private sorted = [];
  private apua = {counter: 0, date: 0};
  private chartData = [];
  private yearArray = [];
  private dayArray = [];
  private display: boolean;
  private ruuviMac: any;
  private ruuviId: any;
  constructor(private usageService: UsageService ) {

    this.display = false;
   }


   @Input()
   set device(data: any) {

     console.log("apua ");
     console.log(data);
     this.ruuviMac = data.ruuviTag;
     this.ruuviId = data.id;
     this.getUsage(this.ruuviMac);
     this.barChartData[0].label = "Motikka " + this.ruuviId;
   }
  ngOnInit() {
    
  }
  
  private onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

  private getUsage(data): Promise<void>{
    return this.usageService.getUsage(data)
      .map(devices => {
        this.devices = devices;
        console.log(this.devices);
        
        //this.sorted = this.groupBy(this.devices,'ruuvitag');
        //this.sorted = this.groupBy(this.devices,'date');
        console.log(this.sorted);
        
        for(var j in this.devices){
        
          if(this.devices[j].data == "None")continue;
          var jsonString = JSON.parse(this.devices[j].data);
          var dateData = {year: [], month:[]};
   
            for(var i in jsonString){
              if(jsonString[i].counter > 0){
                var c = jsonString[i].counter / 1800;
                c = c * 100;
                c = Math.round(c * 10)/10;
                //console.log(jsonString[i].timestamp + ": " + c+"%");
                jsonString[i].counter = c;
                this.lineChartData[0].data.push(parseFloat(jsonString[i].counter));
                this.lineChartLabels.push(jsonString[i].timestamp);

                var month = jsonString[i].timestamp.slice(3,5);
                var year = jsonString[i].timestamp.slice(6,8);
            
                var dataObj = {month:0,year:0};
                dataObj.year = parseInt(jsonString[i].timestamp.slice(6,8));

                //Kuukausi näkymä
                var day = parseInt(jsonString[i].timestamp.slice(0,2));
            
                dataObj.month = parseInt(jsonString[i].timestamp.slice(3,5));
                dataObj.month = dataObj.month -1;
                this.chartData.push(dataObj.month);
                this.yearArray.push(dataObj.year);
                
      
                if(this.dayArray.includes(day) == false){

                  this.dayArray.push(day);
                  this.barChartData[0].data[dataObj.month]++;
                  console.log('Läpi: ' + day);
          
                }         
              }
            }
            var labelArray = [];
            var sortedYearArray = this.yearArray.filter(this.onlyUnique);
            
            this.display = true;

            this.chartData.sort(function(a, b){return a.month - b.month});
      
      
            //Vuosi dropdown
            /*
            var x = document.getElementById("barSelect");
            for(var t in sortedYearArray){
              var option = document.createElement("option");
              option.text = sortedYearArray[t];
              option.value = sortedYearArray[t];
              x.appendChild(option);
            }*/

          //this.updateChart();
          

          
          }

            })
              .toPromise()
              .catch(error => {
              console.log(error);
              })

      
  }
  

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
    
  };
  public barChartLabels:string[] = ['Tammi','Helmi','Maalis','Huhti','Touko','Kesä','Heinä','Elo','Syys','Loka','Marras','Joulu'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  
 
  public barChartData:any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Mötikkä',
    backgroundColor: [
      'rgba(0, 99, 132, 0.6)',
      'rgba(30, 99, 132, 0.6)',
      'rgba(60, 99, 132, 0.6)',
      'rgba(90, 99, 132, 0.6)',
      'rgba(120, 99, 132, 0.6)',
      'rgba(150, 99, 132, 0.6)',
      'rgba(180, 99, 132, 0.6)',
      'rgba(210, 99, 132, 0.6)',
      'rgba(240, 99, 132, 0.6)'
    ]}
  ];
    
    // lineChart alkaa

    public lineChartData:Array<any> = [
      {data: [] , label: 'Möträk'}/*,
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}/*,
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}*/
    ];
    public lineChartLabels:Array<any> = [];
    public lineChartOptions:any = {
      responsive: true
    };
    public lineChartColors:Array<any> = [
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      },
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
    ];
    public lineChartLegend:boolean = true;
    public lineChartType:string = 'line';
  
    /*public updateChart():void{
      let _lineChartData:Array<any> = new Array(this.lineChartData.length);
      _lineChartData = this.lineChartData;
      this.lineChartData = _lineChartData;
    }*/
   
   /* public randomize():void {
      let _lineChartData:Array<any> = new Array(this.lineChartData.length);
      for (let i = 0; i < this.lineChartData.length; i++) {
        _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
        for (let j = 0; j < this.lineChartData[i].data.length; j++) {
          _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
        }
      }
      this.lineChartData = _lineChartData;
    }*/
   
    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
   
    public chartHovered(e:any):void {
      console.log(e);
    }
    // Line chart päättyy
}
